mocks = angular.module 'mocks', []

mocks.factory 'homeMock', [ () ->
    aboutUs: "Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.Proingravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum"
]

mocks.factory 'dogsMock', [ () ->
   dogs: [{
    id: 1
    name : 'Beast Wish Comes True'
    gender: 'Male'
    dateOfBirth: '27.12.2012'
    vendor: 'Paula Georgescu'
    owner: 'Paula Georgescu'
    dogInfo: 'Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor,Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor,Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor,'
    profilePicName: 'profile_pic.jpg'
    profileAlt: 'Beastie'
    trophies: [{
      showName: 'CAC'
      result: 'Excelent'
      location: 'Cluj Napoca, Romania'
      date: '27.12.2014'
      }]
    album: [{
        picName: '1.jpg'
        alt: 'mock1'
      }
      {
        picName: '2.jpg'
        alt: 'mock2'
      }
      {
        picName: '3.jpg'
        alt: 'mock2'
      }]
   }

   {
    id: 2
    name : 'Beast Wish Comes True'
    gender: 'Male'
    dateOfBirth: '27.12.2012'
    vendor: 'Paula Georgescu'
    owner: 'Paula Georgescu'
    dogInfo: 'Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor,Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor,Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor,'
    profilePicName: 'profile_pic.jpg'
    profileAlt: 'Beastie'
    trophies: [{
      showName: 'CAC'
      result: 'Excelent'
      location: 'Cluj Napoca, Romania'
      date: '27.12.2014'
      }]
    album: [{
        picName: '1.jpg'
        alt: 'mock1'
      }
      {
        picName: '2.jpg'
        alt: 'mock2'
      }
      {
        picName: '3.jpg'
        alt: 'mock2'
      }]
   }

   {
    id: 3
    name : 'Beast Wish Comes True'
    gender: 'Female'
    dateOfBirth: '27.12.2012'
    vendor: 'Paula Georgescu'
    owner: 'Paula Georgescu'
    dogInfo: 'Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor,Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor,Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor,'
    profilePicName: 'profile_pic.jpg'
    profileAlt: 'Beastie'    
    trophies: [{
      showName: 'CAC'
      result: 'Excelent'
      location: 'Cluj Napoca, Romania'
      date: '27.12.2014'
      }]
    album: [{
        picName: '1.jpg'
        alt: 'mock1'
      }
      {
        picName: '2.jpg'
        alt: 'mock2'
      }
      {
        picName: '3.jpg'
        alt: 'mock2'
      }]
   }

   {
    id: 4
    name : 'Beast Wish Comes True'
    gender: 'Female'
    dateOfBirth: '27.12.2012'
    vendor: 'Paula Georgescu'
    owner: 'Paula Georgescu'
    dogInfo: 'Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor,Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor,Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor,'
    profilePicName: 'profile_pic.jpg'
    profileAlt: 'Beastie'
    trophies: [{
      showName: 'CAC'
      result: 'Excelent'
      location: 'Cluj Napoca, Romania'
      date: '27.12.2014'
      }
    {
      showName: 'CACIB'
      result: 'Excelent'
      location: 'Timisoara, Romania'
      date: '27.12.2004'
      }
    ]
    album: [{
        picName: '1.jpg'
        alt: 'mock1'
      }
      {
        picName: '2.jpg'
        alt: 'mock2'
      }
      {
        picName: '3.jpg'
        alt: 'mock2'
      }]
   }]

]

mocks.factory 'littersMock', [ () ->
  litters: [{
    id: 1
    letter: 'A'
    litterDate: '27.12.2014'
    mother: 'Beastie'
    father: 'Beastie'
    malePuppies: 10
    femalePuppies: 3
    statistics : {
      fawn: {
        number: 1
        percentage: 10
        }
      blue: {
        number: 2
        percentage: 20
        }
      brindle: {
        number: 3
        percentage: 30
        }
      black: {
        number: 4
        percentage: 40
        }
      harlequin: {
        number: 5
        percentage: 50
        }
      mantle: {
        number: 6
        percentage: 60
        }
      merle: {
        number: 7
        percentage: 70
       }
      }
    litterInfo: {
       #includes/imgs/litters/{{litterLetter}}/album/..
       text: 'Place for posting litter additional information or just saying some things about the parents or the puppies.'
       album: [{
          picName: '1.jpg'
          alt: 'mock1'
        }
        {
          picName: '2.jpg'
          alt: 'mock2'
        }
        {
          picName: '3.jpg'
          alt: 'mock3'
        }]
    }
  }
  {
    id: 2
    letter: 'B'
    litterDate: '27.12.2014'
    mother: 'Beastie'
    father: 'Beastie'
    malePuppies: 10
    femalePuppies: 3
    statistics : {
      fawn: {
        number: 3
        percentage: 10
        }
      blue: {
        number: 3
        percentage: 10
        }
      brindle: {
        number: 3
        percentage: 10
        }
      black: {
        number: 3
        percentage: 10
        }
      harlequin: {
        number: 3
        percentage: 10
        }
      mantle: {
        number: 3
        percentage: 10
        }
      merle: {
        number: 3
        percentage: 10
       }
      }
    puppies: [{
      #includes/imgs/litters/{{litterLetter}}/puppies/{{puppy_id}}/..
      id: 1
      owner: 'Paula Georgescu'
      vendor: 'Paula Georgescu'
      color: 'Blue'
      gender: 'Male'
      availableStatus: true
      additionalInfo: 'This is our first puppy in the litter. He is sold, but beautiful, so I posted some pics'
      album: [{
        picName: '1.jpg'
        alt: 'mock'
        }
        {
        picName: '2.jpg'
        alt: 'mock'
        }
        {
        picName: '3.jpg'
        alt: 'mock'
        }]
      }
      {
      id: 2
      color: 'Blue'
      gender: 'Female'
      additionalInfo: 'This is the second puppy in our litter. He is available'
      availableStatus: false
      album: [{
        picName: '1.jpg'
        alt: 'mock'
        }
        {
        picName: '2.jpg'
        alt: 'mock'
        }
        {
        picName: '3.jpg'
        alt: 'mock'
        }]
      }]  
  }
  {
    id: 3
    letter: 'C'
    litterDate: '27.12.2014'
    mother: 'Beastie'
    father: 'Beastie'
    malePuppies: 10
    femalePuppies: 3
    statistics : {
      fawn: {
        number: 3
        percentage: 10
        }
      blue: {
        number: 3
        percentage: 10
        }
      brindle: {
        number: 3
        percentage: 10
        }
      black: {
        number: 3
        percentage: 10
        }
      harlequin: {
        number: 3
        percentage: 10
        }
      mantle: {
        number: 3
        percentage: 10
        }
      merle: {
        number: 3
        percentage: 10
       }
      }
    litterInfo: {
       #includes/imgs/litters/{{litterLetter}}/album/..
       text: 'In the left we have pictures with  the full litter, the mother and her pupps'
       album: [{
          picName: '1.jpg'
          alt: 'mock1'
        }
        {
          picName: '2.jpg'
          alt: 'mock2'
        }
        {
          picName: '3.jpg'
          alt: 'mock3'
        }]
    }
    puppies: [{
      id: 1
      color: 'Black'
      gender: 'Female'
      availableStatus: false
      additionalInfo: 'This is the first puppy in our litter. He is sold but I wanted to show some picuters of him'
      album: [{
        picName: '1.jpg'
        alt: 'mock'
        }
        {
        picName: '2.jpg'
        alt: 'mock'
        }
        {
        picName: '3.jpg'
        alt: 'mock'
        }]
      }
      {
      id: 2
      color: 'Blue'
      gender: 'Female'
      availableStatus: true
      additionalInfo: 'This is the second puppy in our litter. He is available'
      album: [{
        picName: '1.jpg'
        alt: 'mock'
        }
        {
        picName: '2.jpg'
        alt: 'mock'
        }
        {
        picName: '3.jpg'
        alt: 'mock'
        }] 
      }] 
  }]
]

mocks.factory 'newsMock', [ () ->
  latestNewsLimit: 7
  allNews: [{
    id: 1
    title: 'First post - simple no album no video'
    date: 1288323623006
    thumbnail: 'thumbnail.jpg'
    thumbnailAlt: ''
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu nisi ac mi malesuada vestibulum. Phasellus tempor nunc eleifend cursus molestie. Mauris lectus arcu, pellentesque at sodales sit amet, condimentum id nunc. Donec ornare mattis suscipit. Praesent fermentum accumsan vulputate. Sed velit nulla, sagittis non erat id, dictum vestibulum ligula. '  
    }
    {
    id: 2
    title: 'Second Post - just album'
    date: 1288323623006
    thumbnail: 'thumbnail.jpg'
    thumbnailAlt: ''
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu nisi ac mi malesuada vestibulum. Phasellus tempor nunc eleifend cursus molestie. Mauris lectus arcu, pellentesque at sodales sit amet, condimentum id nunc. Donec ornare mattis suscipit. Praesent fermentum accumsan vulputate. Sed velit nulla, sagittis non erat id, dictum vestibulum ligula. '
    album: [{
        picName: '3.jpg'
        alt: 'alt1'
      }
      {
        picName: '2.jpg'
        alt: 'alt'
      }]
    }
    {
    id: 3
    title: 'Third one - has one video'
    date: 1288323623006
    thumbnail: 'thumbnail.jpg'
    thumbnailAlt: ''
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu nisi ac mi malesuada vestibulum. Phasellus tempor nunc eleifend cursus molestie. Mauris lectus arcu, pellentesque at sodales sit amet, condimentum id nunc. Donec ornare mattis suscipit. Praesent fermentum accumsan vulputate. Sed velit nulla, sagittis non erat id, dictum vestibulum ligula. '
    videos: [{
      url: 'http://www.youtube.com/embed/vWJorwEQWLk'
      }]
    }
    {
    id: 4
    title: 'Fourth one - has two videos'
    date: 1288323623006
    thumbnail: 'thumbnail_default.png'
    thumbnailAlt: ''
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu nisi ac mi malesuada vestibulum. Phasellus tempor nunc eleifend cursus molestie. Mauris lectus arcu, pellentesque at sodales sit amet, condimentum id nunc. Donec ornare mattis suscipit. Praesent fermentum accumsan vulputate. Sed velit nulla, sagittis non erat id, dictum vestibulum ligula. '
    videos: [{
        url: 'http://www.youtube.com/embed/vWJorwEQWLk'
      }
      {
        url: 'http://www.youtube.com/embed/vWJorwEQWLk'
      }]
    }
    {
    id: 5
    title: 'Last one - has videos and pics'
    date: 1288323623006
    thumbnail: 'thumbnail_default.png'
    thumbnailAlt: ''
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu nisi ac mi malesuada vestibulum. Phasellus tempor nunc eleifend cursus molestie. Mauris lectus arcu, pellentesque at sodales sit amet, condimentum id nunc. Donec ornare mattis suscipit. Praesent fermentum accumsan vulputate. Sed velit nulla, sagittis non erat id, dictum vestibulum ligula. '
    album: [{
        picName: '1.jpg'
        alt: 'alt1'
      }
      {
        picName: '2.jpg'
        alt: 'alt'
      }]
    videos: [{
        url: 'http://www.youtube.com/embed/vWJorwEQWLk'
      }
      {
        url: 'http://www.youtube.com/embed/vWJorwEQWLk'
      }]
    }]
]

mocks.factory 'contactMock', [ () ->
  contactData: {
    phoneNumber: '0722222222'
    adress: 'str. I.L Caragiale, nr 14, jud. Timis, loc. Timisoara, Romania'
    email: ''
    mapOptions: {
      adress: '40.717599,-74.005136' 
      zoom: 15
      scroll: true
      streetView: true
    }
  }
]