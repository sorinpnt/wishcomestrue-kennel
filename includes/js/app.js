// Generated by CoffeeScript 1.8.0

/*root = angular.element(document.getElementsByTagName("body"))
watchers = []
f = (element) ->
  angular.forEach [
    "$scope"
    "$isolateScope"
  ], (scopeProperty) ->
    if element.data() and element.data().hasOwnProperty(scopeProperty)
      angular.forEach element.data()[scopeProperty].$$watchers, (watcher) ->
        watchers.push watcher
        return
    return
  angular.forEach element.children(), (childElement) ->
    f angular.element(childElement)
    return
f root
console.log watchers.length
 */

(function() {
  'use strict';
  var wishComesTrue;

  wishComesTrue = angular.module('WishComesTrue', ['ui.router', 'ngAnimate', 'ui.bootstrap', 'smoothScroll', 'mocks']);

  wishComesTrue.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('home');
    return $stateProvider.state('home', {
      url: '/home',
      templateUrl: 'includes/angular-views/home.html',
      controller: 'homeController'
    }).state('our-dogs', {
      url: '/our-dogs',
      templateUrl: 'includes/angular-views/our-dogs.html',
      controller: 'dogsController'
    }).state('litters', {
      url: '/litters',
      templateUrl: 'includes/angular-views/litters.html',
      controller: 'littersController'
    }).state('news', {
      url: '/news',
      templateUrl: 'includes/angular-views/news.html',
      controller: 'newsController'
    }).state('contact', {
      url: '/contact',
      templateUrl: 'includes/angular-views/contact.html',
      controller: 'contactController'
    }).state('404', {
      url: '/404',
      templateUrl: 'includes/angular-views/404.html'
    });
  });

  wishComesTrue.factory('mapInitializer', [
    function() {
      return {
        init: function(mapOptions) {
          return $('#map_canvas').gmap3({
            marker: {
              address: mapOptions.adress
            },
            map: {
              options: {
                zoom: mapOptions.zoom,
                scrollwheel: mapOptions.scroll,
                streetViewControl: mapOptions.streetView
              }
            }
          });
        }
      };
    }
  ]);

  wishComesTrue.controller('homeController', [
    '$scope', 'homeMock', function($scope, mock) {
      $scope.aboutUs = mock.aboutUs;
      return initAnimations();
    }
  ]);

  wishComesTrue.controller('dogsController', [
    '$scope', 'smoothScroll', 'dogsMock', function($scope, smoothScroll, mock) {
      $scope.dogs = mock.dogs;
      $scope.gotoElement = function(eID) {
        var element;
        element = document.getElementById(eID);
        return smoothScroll(element);
      };
      return isotopeInit();
    }
  ]);

  wishComesTrue.controller('littersController', [
    '$scope', 'smoothScroll', 'littersMock', function($scope, smoothScroll, mock) {
      $scope.litters = mock.litters;
      $scope.presentationRows = new Array(Math.ceil($scope.litters.length / 4));
      return $scope.gotoElement = function(eID) {
        var element;
        element = document.getElementById(eID);
        return smoothScroll(element);
      };
    }
  ]);

  wishComesTrue.controller('newsController', [
    '$scope', '$filter', '$animate', 'newsMock', function($scope, $filter, $animate, mock) {
      $scope.allNews = mock.allNews;
      $scope.latestNewsLimit = mock.latestNewsLimit;
      $scope.latestNews = $filter('limitTo')($scope.allNews, $scope.latestNewsLimit);
      $scope.articleHolderCache = $('#article-holder');
      $scope.currentPostIndex = 0;
      $scope.currentPost = $scope.allNews[$scope.currentPostIndex];
      $scope.goToPost = function(postIndex) {
        $scope.currentPostIndex = postIndex;
        return $scope.updateCurrentPost();
      };
      $scope.previousPost = function() {
        if ($scope.currentPostIndex >= 1) {
          $scope.currentPostIndex--;
          return $scope.updateCurrentPost();
        }
      };
      $scope.nextPost = function() {
        if ($scope.currentPostIndex < $scope.allNews.length - 1) {
          $scope.currentPostIndex++;
          return $scope.updateCurrentPost();
        }
      };
      $scope.updateCurrentPost = function() {
        $scope.animatePostExit();
        $scope.animatePostEnter();
        return $scope.loadPostData();
      };
      $scope.animatePostExit = function() {
        return $animate.addClass($scope.articleHolderCache, 'disappear', function() {
          return $animate.removeClass($scope.articleHolderCache, 'disappear', function() {});
        });
      };
      $scope.animatePostEnter = function() {
        return $animate.addClass($scope.articleHolderCache, 'appear', function() {
          return $animate.removeClass($scope.articleHolderCache, 'appear', function() {});
        });
      };
      $scope.loadPostData = function() {
        $scope.currentPost = $scope.allNews[$scope.currentPostIndex];
        if ($scope.currentPost.videos != null) {
          return $scope.$broadcast('updateVideos', $scope.currentPost.videos);
        }
      };
      initAnimations();
      return $scope.updateCurrentPost();
    }
  ]);

  wishComesTrue.controller('contactController', [
    '$scope', 'smoothScroll', 'mapInitializer', 'contactMock', function($scope, smoothScroll, mapInitializer, mock) {
      $scope.contactData = mock.contactData;
      mapInitializer.init($scope.contactData.mapOptions);
      return initAnimations();
    }
  ]);

  wishComesTrue.controller('headerController', ['$scope', function($scope) {}]);

  wishComesTrue.directive("owlCarousel", function() {
    return {
      restrict: "A",
      scope: {
        view: '=',
        pics: '=',
        pid: '='
      },
      link: function(scope, element, attrs) {
        var buttonsContainer, getImagePath, options, pics;
        getImagePath = function(pic) {
          var litterInfo, path;
          path = 'includes/imgs/dogs/' + scope.pid + '/album/' + pic.picName;
          if (scope.view === "littersView-general") {
            path = 'includes/imgs/litters/' + scope.pid + '/album/' + pic.picName;
          }
          if (scope.view === "news") {
            path = 'includes/imgs/news/' + scope.pid + '/album/' + pic.picName;
          }
          if (scope.view === "littersView-specific") {
            litterInfo = scope.pid.split(',');
            path = 'includes/imgs/litters/' + litterInfo[0] + '/puppies/' + litterInfo[1] + '/' + pic.picName;
          }
          return path;
        };
        pics = '';
        angular.forEach(scope.pics, function(key, value) {
          return pics += '<div> <img class="center-block" src="' + getImagePath(key) + '" alt="' + key.alt + '"/> </div>';
        });
        options = {
          slideSpeed: 300,
          paginationSpeed: 400,
          singleItem: true,
          navigation: true
        };
        element.html(pics);
        $(element).owlCarousel(options);
        buttonsContainer = element.find('.owl-buttons');
        buttonsContainer.children('.owl-prev').html('<i class="fa fa-chevron-left"></i>');
        return buttonsContainer.children('.owl-next').html('<i class="fa fa-chevron-right"></i>');
      }
    };
  });

  wishComesTrue.directive('header', function() {
    return {
      restrict: 'A',
      templateUrl: 'includes/partials/header.html',
      transclude: false
    };
  });

  wishComesTrue.directive('appFooter', [
    '$compile', function($compile) {
      return {
        restrict: 'A',
        transclude: false,
        link: function(scope, elem, attr) {
          var outElem;
          outElem = '<div class="footer-copyright"> <div class="container"> <div class="row"> <div class="col-sm-6 col-md-4"> Copyright &copy; 2014  Wish Comes True &nbsp;| &nbsp;All Rights Reserved </div> </div> </div> </div>';
          return elem.html($compile(outElem)(scope));
        }
      };
    }
  ]);

  wishComesTrue.directive('newsVideo', [
    function() {
      return {
        restrict: 'A',
        transclude: false,
        link: function(scope, elem, attr) {
          scope.$on('updateVideos', function(event, videos) {
            var elemClass, outElem;
            outElem = '';
            elemClass = 'col-sm-6 multiple-videos';
            if (videos.length === 1) {
              elemClass = 'col-sm-12 single-video';
            }
            angular.forEach(videos, function(value, key) {
              outElem += '<div class="' + elemClass + '">';
              outElem += '<iframe class="alignnone video-holder center-block margin-bottom-xl" frameborder="0" allowfullscreen src="' + value.url + '"></iframe>';
              return outElem += '</div>';
            });
            return elem.html(outElem);
          });
        }
      };
    }
  ]);

  wishComesTrue.directive('litters', [
    '$compile', function($compile) {
      return {
        restrict: 'A',
        transclude: true,
        scope: {
          litters: '=littersInfo'
        },
        link: function(scope, elem, attr) {
          var litter, litterIndex, outElem, totalPupp, totalPupps, _i, _len, _ref;
          outElem = '';
          _ref = scope.litters;
          for (litterIndex = _i = 0, _len = _ref.length; _i < _len; litterIndex = ++_i) {
            litter = _ref[litterIndex];
            totalPupp = litter.femalePuppies + litter.malePuppies;
            totalPupps = totalPupp.toString();
            outElem += '<div class="container margin-top-xl specific-puppy-section">';
            outElem += '<div class="title-bordered" data-animation="fadeInUp" data-animation-delay="0" id="' + litter.letter + '">' + '<h2> Litter <span>' + litter.letter + '</span></h2> </div>';
            outElem += '<div class="row"> <div class="col-md-4"> <h3>Litter Information</h3> <div class="list list__single-info"> <ul> <li><span>Male:</span> ' + litter.father + ' </li> <li><span>Female:</span> ' + litter.mother + ' </li> <li><span>Date of Birth:</span> ' + litter.litterDate + ' </li> <li><span>Puppies:</span> ' + totalPupps + ' </li> <li><span>Males:</span> ' + litter.malePuppies + ' </li> <li><span>Females:</span> ' + litter.femalePuppies + ' </li> </ul> </div> </div> <div class="col-md-8 litter-statistics"> <h3> Litter Colours</h3> <div class="row"> <div class="col-sm-2 col-xs-6"> <img alt="Fawn Great Dane" src="includes/imgs/patterns/fawn.png" class="center-block border-radius"/> </div> <div class="col-sm-2 col-xs-6"> <h6>Fawn</h6><p><span>' + litter.statistics.fawn.number + '/' + totalPupps + '</span>(' + litter.statistics.fawn.percentage + '%)</p> </div> <div class="col-sm-2 col-xs-6"> <img alt="Blue Great Dane" src="includes/imgs/patterns/blue.png" class="center-block border-radius"/> </div> <div class="col-sm-2 col-xs-6"> <h6>Blue</h6><p><span>' + litter.statistics.blue.number + '/' + totalPupps + '</span>(' + litter.statistics.blue.percentage + '%)</p> </div> <div class="col-sm-2 col-xs-6"> <img alt="Brindle Great Dane" src="includes/imgs/patterns/brindle.png" class="center-block border-radius"/> </div> <div class="col-sm-2 col-xs-6"> <h6>Brindle</h6><p><span>' + litter.statistics.brindle.number + '/' + totalPupps + '</span>(' + litter.statistics.brindle.percentage + '%)</p> </div> </div> <div class="row margin-top margin-bottom"> <div class="col-sm-2 col-xs-6"> <img alt="Black Great Dane" src="includes/imgs/patterns/black.png" class="center-block border-radius"/> </div> <div class="col-sm-2 col-xs-6"> <h6>Black</h6><p><span>' + litter.statistics.black.number + '/' + totalPupps + '</span>(' + litter.statistics.black.percentage + '%)</p> </div> <div class="col-sm-2 col-xs-6"> <img alt="Harlequin Great Dane" src="includes/imgs/patterns/harlequin.png" class="center-block border-radius"/> </div> <div class="col-sm-2 col-xs-6"> <h6>Harlequin</h6><p><span>' + litter.statistics.harlequin.number + '/' + totalPupps + '</span>(' + litter.statistics.harlequin.percentage + '%)</p> </div> <div class="col-sm-2 col-xs-6"> <img alt="Mantle Great Dane" src="includes/imgs/patterns/mantle.png" class="center-block border-radius"/> </div> <div class="col-sm-2 col-xs-6"> <h6>Mantle</h6><p><span>' + litter.statistics.mantle.number + '/' + totalPupps + '</span>(' + litter.statistics.mantle.percentage + '%)</p> </div> </div> <div class="row"> <div class="col-sm-2 col-xs-6"> <img alt="Merle Great Dane" src="includes/imgs/patterns/merle.png" class="center-block border-radius"/> </div> <div class="col-sm-2 col-xs-6"> <h6>Merle</h6><p><span>' + litter.statistics.merle.number + '/' + totalPupps + '</span>(' + litter.statistics.merle.percentage + '%)</p> </div> </div> </div> </div>';
            if (litter.litterInfo != null) {
              outElem += '<div class="row"> <h3> Litter Pictures </h3> <div class="col-md-8 margin-bottom"> <div class="slider-frame center-block"> <div class="owl-carousel" view="\'littersView-general\'" pics=\'' + JSON.stringify(litter.litterInfo.album) + '\' pid="\'' + litter.letter.toString() + '\'" owl-carousel></div> </div> </div> <div class="col-md-4 pad-top-xxl"> <p>' + litter.litterInfo.text + ' </p> </div> </div>';
            }
            if (litter.puppies != null) {
              outElem += '  <div class="row" litter-puppies letter="\'' + litter.letter + '\'" puppies=\'' + JSON.stringify(litter.puppies) + '\'></div>';
            }
            outElem += '</div>';
          }
          elem.html($compile(outElem)(scope));
          initTitleAnimation();
          return initAnimations();
        }
      };
    }
  ]);

  wishComesTrue.directive('litterPuppies', [
    '$compile', '$filter', function($compile, $filter) {
      return {
        restrict: 'A',
        scope: {
          puppies: '=',
          letter: '='
        },
        link: function(scope, elem, attr) {
          var accordionId, outElem, puppy, puppyAdditionalDetails, puppyIndex, _i, _len, _ref;
          accordionId = 'accordion_' + scope.letter;
          outElem = '<div class="col-md-12 puppy-accordion pad-bottom-xl"> <h3> Litter Puppies</h3>';
          outElem += '    <div class="panel-group panel-group__alt" id="' + accordionId + '">';
          _ref = scope.puppies;
          for (puppyIndex = _i = 0, _len = _ref.length; _i < _len; puppyIndex = ++_i) {
            puppy = _ref[puppyIndex];
            puppyAdditionalDetails = '';
            if (puppy.owner != null) {
              puppyAdditionalDetails += '<li><span>Owner:</span> ' + puppy.owner + ' </li>';
            }
            if (puppy.vendor != null) {
              puppyAdditionalDetails += '<li><span>Owner:</span> ' + puppy.vendor + ' </li>';
            }
            outElem += '    <div class="panel panel-default"> <div class="panel-heading"> <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#' + accordionId + '" data-target="#' + accordionId + '_' + puppyIndex.toString() + '">' + puppy.gender + ', ' + puppy.color + ' # ' + (puppyIndex + 1).toString() + '<div class="availabilty pull-right"> Available : ' + $filter('puppyAvilability')(puppy.availableStatus) + ' </div> </a> </h4> </div> <div id="' + accordionId + '_' + puppyIndex.toString() + '" class="panel-collapse collapse"> <div class="panel-body"> <div class="col-sm-offset-1 col-sm-6"> <div class="slider-frame center-block"> <div class="owl-carousel" view="\'littersView-specific\'" pics=\'' + JSON.stringify(puppy.album) + '\' pid="\'' + scope.letter + ',' + puppy.id.toString() + '\'" owl-carousel></div> </div> </div> <div class="col-sm-offset-1 col-sm-3"> <div class="list list__single-info"> <ul> <li><span>Colour:</span> ' + puppy.color + ' </li> <li><span>Gender:</span> ' + puppy.gender + ' </li>' + puppyAdditionalDetails + '</ul> <p>' + puppy.additionalInfo + ' </p> </div> </div> </div> </div> </div>';
          }
          outElem += '    </div> </div>';
          return elem.html($compile(outElem)(scope));
        }
      };
    }
  ]);

  wishComesTrue.filter('littersOverviewRowSet', function() {
    return function(allLiters, rowNumber) {
      return allLiters.slice(rowNumber * 4, (rowNumber * 4) + 4);
    };
  });

  wishComesTrue.filter('puppyAvilability', function() {
    return function(status) {
      var faClass;
      faClass = 'fa-times text-danger';
      if (status) {
        faClass = 'fa-check text-success';
      }
      return '<div class="fa ' + faClass + '"></div>';
    };
  });

}).call(this);
