jQuery(function($){
  $('#footer .footer-copyright').append('<div id="back-top"><a href="#top"><i class="fa fa-chevron-up"></i></a></div>')
  $('#back-top a').click(function(e) {
      e.preventDefault();
      $('body,html').animate({
          scrollTop: 0
      }, 400);
      return false;
  });

  isotopeInit = function() {
    var $container          = $('.project-feed');
    var $filter             = $('.project-feed-filter');
    $(window).smartresize(function(){
        $container.isotope({
            filter              : '*',
            resizable           : true,
            layoutMode: 'sloppyMasonry',
            itemSelector: '.project-item'
        });
    });
    $container.imagesLoaded( function(){
        $(window).smartresize();
    });

    $filter.find('a').click(function() {
        var selector = $(this).attr('data-filter');
        $filter.find('a').removeClass('btn-primary');
        $(this).addClass('btn-primary');
        $container.isotope({ 
            filter             : selector,
            animationOptions   : {
            animationDuration  : 750,
            easing             : 'linear',
            queue              : false
            }
        });
        return false;
    });
  };

  initTitleAnimation = function() {
    $('.title-bordered h2').append('<span class="line line__right"></span>').prepend('<span class="line line__left"></span>');
  }

  initAnimations = function() {
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    if (isMobile == false) {
      $("[data-animation]").each(function() {
        var $this = $(this);
        $this.addClass("animation");
        if(!$("html").hasClass("no-csstransitions") && $(window).width() > 767) {
          $this.appear(function() {
            var delay = ($this.attr("data-animation-delay") ? $this.attr("data-animation-delay") : 1);
            if(delay > 1) $this.css("animation-delay", delay + "ms");
            $this.addClass($this.attr("data-animation"));
            setTimeout(function() {
              $this.addClass("animation-visible");
            }, delay);
          }, {accX: 0, accY: -170});
        } else {
          $this.addClass("animation-visible");
        }
      });  
    }
  }
  
});

