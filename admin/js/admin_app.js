// Generated by CoffeeScript 1.8.0
(function() {
  'use strict';
  var wishComesTrueAdmin;

  wishComesTrueAdmin = angular.module('WishComesTrueAdmin', ['ui.router', 'mocks']);

  wishComesTrueAdmin.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('home');
    return $stateProvider.state('home', {
      url: '/home',
      templateUrl: 'angular-views/home.html',
      controller: 'adminHomeController'
    }).state('dogs', {
      url: '/dogs',
      templateUrl: 'angular-views/dogs.html',
      controller: 'adminDogsController'
    }).state('litters', {
      url: '/litters',
      templateUrl: 'angular-views/litters.html',
      controller: 'adminLittersController'
    }).state('contact', {
      url: '/contact',
      templateUrl: 'angular-views/contact.html',
      controller: 'adminContactController'
    }).state('news', {
      url: '/news',
      templateUrl: 'angular-views/news.html',
      controller: 'adminNewsController'
    });
  });

  wishComesTrueAdmin.controller('adminHomeController', [
    '$scope', '$rootScope', 'homeMock', function($scope, $rootScope, mock) {
      $scope.aboutUs = mock.aboutUs;
      return $scope.saveChange = function() {
        return $rootScope.$emit('showMessage', {
          type: 'danger',
          text: 'Feature not yet implemented'
        });
      };
    }
  ]);

  wishComesTrueAdmin.controller('adminDogsController', ['$scope', function($scope) {}]);

  wishComesTrueAdmin.controller('adminLittersController', ['$scope', function($scope) {}]);

  wishComesTrueAdmin.controller('adminContactController', ['$scope', function($scope) {}]);

  wishComesTrueAdmin.controller('adminNewsController', ['$scope', function($scope) {}]);

  wishComesTrueAdmin.controller('headerController', [
    '$scope', function($scope) {
      return $scope.user = 'Andrei Ghidon';
    }
  ]);

  wishComesTrueAdmin.directive('header', function() {
    return {
      restrict: 'A',
      controller: 'headerController',
      template: '<div flash-message></div> <div class="container margin-top"> <div class="row"> <div class="header-top-right pull-right"> <span class="register">Welcome, <span class="user text-success"> {{ user }} </span> <a class="btn btn-sm btn-danger margin-left" href="#">Sign Out</a></span> </div> </div> <div class="row margin-bottom"> <ul class="nav nav-tabs"> <li role="presentation" ui-sref-active="active"><a ui-sref="home">Home</a></li> <li role="presentation" ui-sref-active="active"><a ui-sref="dogs">Our Dogs</a></li> <li role="presentation" ui-sref-active="active"><a ui-sref="litters">Litters</a></li> <li role="presentation" ui-sref-active="active"><a ui-sref="contact">Contact</a></li> <li role="presentation" ui-sref-active="active"><a ui-sref="news">News</a></li> </ul> </div> </div>',
      transclude: false
    };
  });

  wishComesTrueAdmin.controller('flashMessageController', [
    '$scope', '$compile', '$rootScope', '$timeout', function($scope, $compile, $rootScope, $timeout) {
      $scope.timerStarted = false;
      $scope.messageBoxId = 0;
      $scope.timer = '';
      $scope.startTimer = function() {
        if ($scope.timerStarted) {
          $scope.cancelTimer();
        }
        $scope.timer = $timeout(function() {
          $scope.messageContainer.html('');
          return false;
        }, 3000);
        return $scope.timerStarted = true;
      };
      $scope.cancelTimer = function() {
        $timeout.cancel($scope.timer);
        return $scope.timerStarted = false;
      };
      $scope.getMessageHeader = function(type) {
        var messageHeader;
        messageHeader = type.charAt(0).toUpperCase() + type.slice(1) + ' ! ';
        if (type === 'danger') {
          messageHeader = "Error ! ";
        }
        if (type === 'info') {
          messageHeader = '';
        }
        return messageHeader;
      };
      $scope.getTemplate = function(type, message, currentMessageIndex) {
        $scope.messageBoxId++;
        return '<div id="messageBox' + $scope.messageBoxId.toString() + '"><div class="alert alert-dismissible alert-' + type + ' no-margin" role="alert"> <button type="button" ng-click="dissmissMessage(\'messageBox' + $scope.messageBoxId.toString() + '\')" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>' + $scope.getMessageHeader(type) + ' </strong>' + message + ' </div>';
      };
      $scope.dissmissMessage = function(elementId) {
        elementId = '#' + elementId;
        $(elementId).remove();
        return true;
      };
      return $rootScope.$on('showMessage', function(event, messageInfo) {
        $scope.messageContainer.prepend($compile($scope.getTemplate(messageInfo.type, messageInfo.text))($scope));
        $scope.startTimer();
        return false;
      });
    }
  ]);

  wishComesTrueAdmin.directive('flashMessage', [
    '$compile', '$rootScope', function($compile, $rootScope) {
      return {
        restrict: 'A',
        controller: 'flashMessageController',
        transclude: false,
        link: function($scope, elem, attr) {
          var elemTemplate;
          elemTemplate = '<div id="messageContainer"></div>';
          elem.html(elemTemplate);
          return $scope.messageContainer = $('#messageContainer');
        }
      };
    }
  ]);

}).call(this);
